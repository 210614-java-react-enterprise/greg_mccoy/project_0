# Project_0

This project is located in the master branch of this repository.

This is a Banking Application that was developed using Java and OOP.
This Banking Application allows a user to register an account, or log into an account.
From there a user is able to view Customer Information and make changes to that information.
The user is also able to interact with the banking accounts by withdrawing and depositing money.
After every transaction a transaction is recorded and the user is able to view all transactions
associated with that account.

Things to have:

- Database(Postgres)
- Dbeaver(Good for visualizing and interacting with Database)
- Credentials as Environment variables
- Maven
- JDBC(Java Database Connectivity)
